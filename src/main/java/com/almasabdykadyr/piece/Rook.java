package com.almasabdykadyr.piece;

import com.almasabdykadyr.Color;
import com.almasabdykadyr.Coordinates;
import java.util.Set;

public class Rook extends LongRangePiece implements IRook {
    public Rook(Color color, Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getPieceMoves() {
        return getRookMoves();
    }
}
