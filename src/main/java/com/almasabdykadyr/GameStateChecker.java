package com.almasabdykadyr;

import com.almasabdykadyr.board.Board;

public abstract class GameStateChecker {
    public abstract GameState check(Board board, Color color);
}
